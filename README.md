# Steps

1. <a href="https://gitlab.com/carlosaospina/snippets-steps/blob/master/oh_my_zsh.md" target="_blank">Oh My Zsh shell & Git</a>. En Ubuntu reemplaza Bash por una terminal más flexible y especial para Git.
2. <a href="https://gitlab.com/carlosaospina/snippets-steps/blob/master/sublime_text_3.md" target="_blank">Sublime Text 3</a>
4. <a href="https://gitlab.com/carlosaospina/snippets-steps/blob/master/python-postgresql.md" target="_blank">PostgreSQL + Python (Ubuntu)</a>
5. <a href="https://gitlab.com/carlosaospina/snippets-steps/blob/master/anaconda3.md" target="_blank">Anaconda</a>
6. <a href="https://gitlab.com/carlosaospina/snippets-steps/blob/master/google_chrome_stable.md" target="_blank">Google Chrome stable version</a>
7. <a href="https://gitlab.com/carlosaospina/snippets-steps/blob/master/igraph.md" target="_blank">Igraph for Python</a>
8. <a href="https://gitlab.com/carlosaospina/snippets-steps/blob/master/java_development_kit.md" target="_blank">Java Development Kit (JDK)</a>
