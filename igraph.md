# Igraph (Ubuntu)
## Requerimientos
```
sudo apt-get -y install python3-pip python3-dev build-essential clang libxml2-dev
sudo pip3 install --upgrade pip
```
## Descarga
```
wget http://igraph.org/nightly/get/c/igraph-0.7.1.tar.gz -P /tmp/ && tar zxvf /tmp/igraph-* -C ~/
```
## Instalación
```
cd ~/igraph-0.7.1/
./configure
sudo make; sudo make install
sudo ldconfig
sudo pip3 install igraph python-igraph
```
En caso de que Python no reconozca ```igraph```, añadir la siguiente línea al final del archivo ```~/.bashrc``` o según sea el caso.
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
```
## Código ejemplo
```python
import igraph as ig


a = ig.Graph(directed=True)
a.add_vertices(12)
a.add_edges([
    (0, 1), (0, 4), (0, 3), (9, 4), (9, 5), (9, 8), (11, 8), (11, 6), (11, 10),
    (7, 3), (7, 2), (7, 6), (7, 10), (4, 1), (1, 5), (1, 8), (5, 8), (2, 6),
    (2, 1), (2, 3), (6, 8), (6, 10)
])
ig.plot(a)
```
