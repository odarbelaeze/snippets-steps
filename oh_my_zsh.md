# Install **Oh My Zsh** shell

```
sudo apt-get update && sudo apt-get install -y curl vim git zsh
curl -L http://install.ohmyz.sh | sh
sudo chsh -s $(which zsh) $(whoami)
```
