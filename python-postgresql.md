Title:  PostgreSQL in Python (Ubuntu)
Author: Carlos A. Ospina
# PostgreSQL installing for python
## <a href="http://www.enterprisedb.com/products-services-training/pgdownload" target="_blank">Download graphic installer<a/>
```
sudo chmod +x <package_path>
sudo ./<package_path>
```
Replacing `<package_path>` by the name of the downloaded package.
```
sudo apt-get install pgadmin3; sudo apt-get -y remove libpq5
sudo apt-get install libpq-dev
sudo apt-get -y install python3-pip python3-dev build-essential
sudo pip3 install --upgrade pip
sudo pip3 install psycopg2
```
To work in anaconda
```
conda install -c https://conda.anaconda.org/anaconda psycopg2
```

# Code example
Importing package
```python
import psycopg2 as pg
```
Connecting to database `example`
```python
conn = pg.connect(database="example", host="localhost", user="postgres", password="pw_example")
```
After each transaction do
```python
conn.commit()
```
Creating tables
```python
with conn.cursor() as c:
    c.execute("CREATE TABLE person (name text, id char(10) NOT NULL)")
    c.execute("CREATE TABLE student (code char(7) NOT NULL, career text) INHERITS (person)")
conn.commit()
```
Inserting records
```python
with conn.cursor() as c:
    c.execute("INSERT INTO person (name, id) VALUES ('pablo', '1000000001')")
    c.execute("INSERT INTO person VALUES ('paco', '1000000002')")
    c.execute("INSERT INTO student (name, id, code, career) VALUES ('lucio', '1000000003', '1000001', 'ASI')")
    c.execute("INSERT INTO student VALUES ('lucas', '1000000004', '1000002', 'ASI')")
conn.commit()
```
Showwing data (SELECT query)
```python
print("All persons")
with conn.cursor() as c:
    c.execute("SELECT name FROM person")
    for record in c:
        print(tuple(record))
    conn.commit()
    print("All students")
    c.execute("SELECT name FROM student")
    for record in c:
        print(tuple(record))
conn.commit()
```
Closing connection
```python
conn.close()
```
# End