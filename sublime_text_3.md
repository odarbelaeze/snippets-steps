# Sublime Text 3
## Download <a href="https://www.sublimetext.com/3", target="_blank">here<a>
```
sudo dpkg -i <filename>
```

---
## Licence key
```
—– BEGIN LICENSE —–
Ryan Clark
Single User License
EA7E-812479
2158A7DE B690A7A3 8EC04710 006A5EEB
34E77CA3 9C82C81F 0DB6371B 79704E6F
93F36655 B031503A 03257CCC 01B20F60
D304FA8D B1B4F0AF 8A76C7BA 0FA94D55
56D46BCE 5237A341 CD837F30 4D60772D
349B1179 A996F826 90CDB73C 24D41245
FD032C30 AD5E7241 4EAA66ED 167D91FB
55896B16 EA125C81 F550AF6B A6820916
—— END LICENSE ——
```
## Package control
View > Show Console...

```
import urllib.request,os,hashlib; h = '261dd1222b4693ce6d4f85f9c827ac06' + '6d5ab8ebdd020086947172a8a1356bb6'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
```
Algunos pasos:
- Ctrl+Shift+P
- Buscar: "Install Package" (Package Control: Install Package)... Entrar
- Buscar los apquetes de interés

### Paquetes importantes:
- Terminal
- Python 3
- Git

## User settings
```sublime-settings
{
	"ignored_packages":
	[
		"Vintage"
	],
	"rulers": [ 79 ],
	"save_on_focus_lost": true,
	"translate_tabs_to_spaces": true
}
```
